const { SerialPort } = require('serialport');                       // https://serialport.io/docs/guide-usage
const { ReadlineParser } = require('@serialport/parser-readline');  // https://serialport.io/docs/api-parser-readline
const express = require('express');             // https://expressjs.com/en/4x/api.html
const {Client} = require('pg');                                     // https://node-postgres.com/
const WebSocket = require('ws');

(async options => {
    const app = express();
    const port = 3000;

    // PostgreSQL connection configuration
    const pgClient = new Client({
        user: 'postgres',
        host: 'localhost',
        database: 'weather_station',
        password: 'postgres',
        port: 5432,
    });

    // Connect to the PostgreSQL database
    await pgClient.connect();
    console.log('Connected to PostgreSQL database');

    const portName = '/dev/ttyUSB0'; // Change this to your actual serial port path
    const serialPort = new SerialPort({
        path: portName,
        baudRate: 9600,
        autoOpen: false
    });

    serialPort.open(function (err) {
        if (err) {
            return console.error(err.message);
        }

        const parser = serialPort.pipe(new ReadlineParser({delimiter: '\n'}))

        parser.on('data', (line) => {
            console.log('Received data:', line);
            const readings = line.split(',');

            const query = {
                text: 'INSERT INTO sensor_readings (value) VALUES ($1)',
                // values: [parseFloat(readings[1])], // Assuming the second value is the sensor reading
                values: [parseInt(readings[0]), parseFloat(readings[1])], // Assuming the first value is the sensor ID and the second is the reading value
            };

            pgClient.query(query)
                .then(() => console.log('Data inserted into PostgreSQL database'))
                .catch((err) => console.error('Error inserting data into PostgreSQL database', err));

            broadcastNewReading({
                sensor_id: parseInt(readings[0]),
                value: parseFloat(readings[1]),
                created_at: new Date().toISOString(),
            });

        });
    });

    app.use(express.static('public'));

    // API endpoint to get sensor readings
    app.get('/api/sensor-readings', async (req, res) => {
        try {
            const result = await pgClient.query('SELECT * FROM sensor_readings ORDER BY created_at DESC LIMIT 1000');
            res.json(result.rows);
        } catch (err) {
            console.error('Error fetching sensor readings', err);
            res.status(500).send('Internal Server Error');
        }
    });

    // API endpoint to get sensor information
    app.get('/api/sensors', async (req, res) => {
        try {
            const result = await pgClient.query('SELECT * FROM sensors');
            res.json(result.rows);
        } catch (err) {
            console.error('Error fetching sensor information', err);
            res.status(500).send('Internal Server Error');
        }
    });

    // Start the server
    const server = app.listen(port, () => {
        console.log(`Server is running on http://localhost:${port}`);
    });

    const wss = new WebSocket.Server({ server });

    function broadcastNewReading(reading) {
        const message = JSON.stringify(reading);
        wss.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(message);
            }
        });
    }

    wss.on('connection', ws => {
        console.log('New WebSocket connection');
        ws.on('message', message => {
            console.log('Received message from client:', message);
        });
    });

})();
