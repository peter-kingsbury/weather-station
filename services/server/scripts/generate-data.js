const {Client} = require('pg');

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'weather_station',
  password: 'postgres',
  port: 5432,
});

const NUM_RECORDS_PER_SENSOR = 100;
const SENSOR_IDS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function generateSensorValue(sensorId) {
  switch (sensorId) {
    case 1: // Temperature (Celsius)
      return (Math.random() * 30).toFixed(2); // 0 to 30 °C
    case 2: // Humidity (%)
      return (Math.random() * 100).toFixed(2); // 0 to 100 %
    case 3: // Pressure (hPa)
      return (980 + Math.random() * 40).toFixed(2); // 980 to 1020 hPa
    case 4: // VOC (ppb)
      return (Math.random() * 500).toFixed(2); // 0 to 500 ppb
    case 5: // Light (Lux)
      return (Math.random() * 10000).toFixed(2); // 0 to 10000 Lux
    case 6: // Dust (µg/m3)
      return (Math.random() * 150).toFixed(2); // 0 to 150 µg/m3
    case 7: // Wind Speed (m/s)
      return (Math.random() * 30).toFixed(2); // 0 to 30 m/s
    case 8: // Wind Direction (Degrees)
      return (Math.random() * 360).toFixed(2); // 0 to 360 Degrees
    case 9: // Rain Level (mm)
      return (Math.random() * 50).toFixed(2); // 0 to 50 mm
    case 10: // Radiation Level (µSv/h)
      return (Math.random() * 2).toFixed(2); // 0 to 2 µSv/h
    default:
      return (Math.random() * 100).toFixed(2); // Default random value
  }
}

async function insertSensorData() {
  try {
    await client.connect();
    console.log('Connected to PostgreSQL database');

    for (const sensorId of SENSOR_IDS) {
      for (let i = 0; i < NUM_RECORDS_PER_SENSOR; i++) {
        const value = generateSensorValue(sensorId);
        const query = {
          text: 'INSERT INTO sensor_readings (sensor_id, value) VALUES ($1, $2)',
          values: [sensorId, value],
        };
        await client.query(query);
      }
      console.log(`Inserted ${NUM_RECORDS_PER_SENSOR} records for sensor_id ${sensorId}`);
    }

  } catch (err) {
    console.error('Error inserting data into PostgreSQL database', err);
  } finally {
    await client.end();
    console.log('Disconnected from PostgreSQL database');
  }
}

insertSensorData();
