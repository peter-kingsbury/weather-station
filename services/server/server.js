const axios = require('axios');
const jpeg = require('jpeg-js');
const fs = require('fs');
const path = require('path');
const buffer = require("node:buffer");

const DOWNLOAD_URL = 'https://sosrff.tsu.ru/new/shm.jpg';
const DB_PATH = process.env.DATABASE_URL || `postgresql://postgres:postgres@localhost:5432/postgres`;
let databaseClient = null;

/**
 * Retrieves the singleton instance of the PostgreSQL database client.
 * Initializes the client if it has not been created yet. Ensures the database
 * connection string is set and connects to the database.
 *
 * @throws {Error} If the database connection string (DB_PATH) is not set.
 * @return {Client} Returns the initialized PostgreSQL database client instance.
 */
async function getDatabaseClient() {
    // Singleton-style database-client getter.

    if (!databaseClient) {
        const {Client} = require('pg');

        // Ensure the database connection string is provided
        if (!DB_PATH) {
            throw new Error('Database connection string (DB_PATH) is not set.');
        }

        // Initialize the database client
        databaseClient = new Client({
            connectionString: DB_PATH,
        });

        // Connect to the database
        await databaseClient.connect();
        console.log('Connected to the PostgreSQL database.');
        // .then(() =>
        // .catch(err => {
        //     console.error('Failed to connect to the database:', err.message);
        //     databaseClient = null; // Reset databaseClient on failure
        // });
    }

    return databaseClient;
}



/**
 * Reads and extracts pixel data from a specific rectangular region of a JPEG image.
 *
 * @param {string} filename - The path to the JPEG image file.
 * @param {number} x1 - The x-coordinate of the top-left corner of the region.
 * @param {number} y1 - The y-coordinate of the top-left corner of the region.
 * @param {number} x2 - The x-coordinate of the bottom-right corner of the region.
 * @param {number} y2 - The y-coordinate of the bottom-right corner of the region.
 * @return {Array<Array<number>>} A 2D array containing RGBA values for each pixel, where each pixel is represented as an array [R, G, B, A].
 * @throws {Error} If the specified coordinates are invalid for the image dimensions.
 */
function readPixelData(filename, x1, y1, x2, y2) {
    // Load the JPEG file and decode it
    const jpegData = fs.readFileSync(filename);
    const rawImageData = jpeg.decode(jpegData);

    console.log(rawImageData);

    // console.log(`width x height = ${rawImageData.width * rawImageData.height * 4}, buffer length = ${rawImageData.data.length}`);

    // process.exit(1);

    const { width, height, data } = rawImageData;

    // Validate coordinates
    if (x1 < 0 || y1 < 0 || x2 > width || y2 > height || x1 >= x2 || y1 >= y2) {
        throw new Error('Invalid coordinates specified for the image dimensions.');
    }

    const pixelData = [];

    // Iterate through the specified rectangular region
    for (let y = y1; y < y2; y++) {
        for (let x = x1; x < x2; x++) {
            const idx = (y * width + x) * 4; // Each pixel has 4 bytes: R, G, B, A
            const r = data[idx];
            const g = data[idx + 1];
            const b = data[idx + 2];
            const a = data[idx + 3];
            pixelData.push([r, g, b, a]);
        }
    }

    return pixelData;
}

/**
 * Generates a timestamp string formatted as "YYYY-MM-DD_HH-mm-ss" for use in filenames.
 *
 * @return {string} A formatted timestamp string representing the current date and time.
 */
function getFilenameTimestamp() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0'); // Months are 0-based
    const day = String(now.getDate()).padStart(2, '0');
    const hours = String(now.getHours()).padStart(2, '0');
    const minutes = String(now.getMinutes()).padStart(2, '0');
    const seconds = String(now.getSeconds()).padStart(2, '0');

    return `${year}-${month}-${day}_${hours}-${minutes}-${seconds}`;
}

/**
 * Downloads a file from a given URL and saves it to a specified directory.
 * @param {string} url - The URL of the file to download.
 * @param {string} savePath - The directory where the file should be saved.
 * @returns {Promise<string>} - The full path of the saved file.
 */
async function downloadFile(url, savePath) {
    try {
        // Extract filename from URL
        const fileName = path.basename(new URL(url).pathname);
        const fullPath = path.join(savePath, `${getFilenameTimestamp()}_${fileName}`);

        // Ensure directory exists
        if (!fs.existsSync(savePath)) {
            fs.mkdirSync(savePath, { recursive: true });
        }

        // Stream the file from the URL
        const response = await axios({
            method: 'get',
            url: url,
            responseType: 'stream',
        });

        // Save the file to the specified path
        const writer = fs.createWriteStream(fullPath);
        response.data.pipe(writer);

        // Return a promise that resolves when writing is complete
        return new Promise((resolve, reject) => {
            writer.on('finish', () => resolve(fullPath));
            writer.on('error', reject);
        });
    } catch (error) {
        console.error(`Error downloading file from ${url}:`, error.message);
        throw error;
    }
}

/**
 * Extracts a vertical strip of pixel data from a JPEG file.
 * @param {string} filename - Path to the JPEG file.
 * @param {number} x - The X coordinate of the vertical strip.
 * @param {number} yStart - Starting Y coordinate (inclusive).
 * @param {number} yEnd - Ending Y coordinate (exclusive).
 * @returns {number[][]} - Array of pixel data in the format [R, G, B, A].
 */
function readVerticalStrip(filename, x, yStart, yEnd) {
    // Load the JPEG file and decode it
    const jpegData = fs.readFileSync(filename);
    const rawImageData = jpeg.decode(jpegData);

    const { width, height, data } = rawImageData;

    // Validate coordinates
    if (x < 0 || x >= width || yStart < 0 || yEnd > height || yStart >= yEnd) {
        throw new Error('Invalid coordinates specified for the image dimensions.');
    }

    const pixelData = [];

    // Iterate through the specified vertical strip
    for (let y = yStart; y < yEnd; y++) {
        const idx = (y * width + x) * 4; // Calculate pixel index
        const r = data[idx];
        const g = data[idx + 1];
        const b = data[idx + 2];
        const a = data[idx + 3];
        pixelData.push([r, g, b, a]);
    }

    return pixelData;
}

/**
 * Matches a pixel to the closest color in a vertical strip and returns a normalized value.
 * @param {number[]} pixel - The input pixel data in the format [R, G, B, A].
 * @param {number[][]} verticalStrip - The vertical strip of pixel data as an array of [R, G, B, A].
 * @param x
 * @param y
 * @returns {number} - A value between 0.0 and 1.0 representing the match.
 */
function matchColorToValue(pixel, verticalStrip, x, y) {
    if (!Array.isArray(pixel) || pixel.length !== 4) {
        console.log(pixel, x, y);
        throw new Error('Pixel data must be an array of [R, G, B, A].');
    }

    if (!Array.isArray(verticalStrip) || verticalStrip.length === 0) {
        throw new Error('Vertical strip must be a non-empty array of [R, G, B, A] pixels.');
    }

    let closestIndex = 0;
    let closestDistance = Infinity;

    for (let i = 0; i < verticalStrip.length; i++) {
        const stripPixel = verticalStrip[i];
        if (!Array.isArray(stripPixel) || stripPixel.length !== 4) {
            throw new Error('Each pixel in the vertical strip must be an array of [R, G, B, A].');
        }

        const distance = Math.sqrt(
            Math.pow(pixel[0] - stripPixel[0], 2) +
            Math.pow(pixel[1] - stripPixel[1], 2) +
            Math.pow(pixel[2] - stripPixel[2], 2)
        );

        if (distance < closestDistance) {
            closestDistance = distance;
            closestIndex = i;
        }
    }

    // Normalize the closest index to a 0.0 to 1.0 range
    return closestDistance === Infinity
        ? 0.0 // Default to 0.0 if no match is found (this shouldn't happen with valid input)
        : closestIndex / (verticalStrip.length - 1);
}

/**
 * Iterates over pixel data and calls a function for each pixel.
 * @param {Date} startDate - The start date for the iteration.
 * @param {number} timeBetweenX - The time difference to add for each pixel in the x-direction.
 * @param {number[][]} pixelData - The pixel data array (2D array of [R, G, B, A]).
 * @param {number[][]} verticalStrip - The vertical strip of pixel data for color matching.
 * @param {function} writeDataPoint - The function to process and store the results (x, y, value).
 */
async function iterateAndMatchColors(startDate, timeBetweenX, pixelData, verticalStrip, writeDataPoint, width, height) {
    const hertzRange = 40.0; // Hertz range (0.0 to 40.0)
    const maxY = pixelData.length; // Assuming pixelData is 2D array with y-values as rows
    let counter = 0;
    const yesterday = getYesterdayDate();

    for (let y = 0; y < height; y++) {
        // Normalize the y-coordinate against the Hertz range (0.0 to 40.0)
        const hertzValue = (y / (height - 1)) * hertzRange;

        for (let x = 0; x < width; x++) {
            // Get the color data from the pixel at (x, y)
            const pixel = pixelData[y * width + x];

            // Match the pixel to a normalized value using the vertical strip
            const normalizedValue = 1.0 - matchColorToValue(pixel, verticalStrip, x, y);

            // Calculate the new date for the x-coordinate based on the timeBetweenX value
            const currentDate = new Date(yesterday.getTime() + timeBetweenX * x);

            // Call the writeDataPoint function to process the data
            await writeDataPoint(currentDate, hertzValue, normalizedValue);

            counter++;
        }
    }

    console.log(`Processed ${counter} data points. (width x height = ${width} x ${height} = ${width * height})`);
}

/**
 * Placeholder function for writing the data point to the database or further processing.
 * @param {Date} timestamp - The timestamp (calculated from the x-coordinate).
 * @param {number} hertzValue - The normalized Hertz value.
 * @param {number} normalizedValue - The matched color value (0.0 to 1.0).
 */
async function writeDataPoint(timestamp, hertzValue, normalizedValue) {
    const client = await getDatabaseClient();
    // For now, log the data (we will integrate database writes later)
    console.log(`Timestamp: ${timestamp.toISOString()}, Hertz: ${hertzValue.toFixed(2)}, Value: ${normalizedValue.toFixed(4)}`);
    await client.query('INSERT INTO schumann_data (time, hertz, reading) VALUES ($1, $2, $3)', [timestamp, hertzValue, normalizedValue]);
}

/**
 * Calculates and returns the date for yesterday with time set to 00:00:00.
 *
 * @return {Date} A Date object representing yesterday at midnight.
 */
function getYesterdayDate() {
    const today = new Date();
    const yesterday = new Date(today);
    yesterday.setDate(yesterday.getDate() - 1);
    yesterday.setHours(0, 0, 0, 0);
    return yesterday;
}

/**
 * Prints the pixel data in a formatted manner, showing the coordinates and respective pixel value.
 *
 * @param {Array} pixelData - A 1D array containing pixel values.
 * @param {number} width - The width of the pixel data grid.
 * @param {number} height - The height of the pixel data grid.
 * @return {void} Does not return a value; logs pixel information to the console.
 */
function printPixelData(pixelData, width, height) {
    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            console.log(`[${x}, ${y}] = ${pixelData[y * width + x]}`);
        }
    }
}


// Example Usage
(async () => {
    const saveDirectory = './downloads'; // Replace with your preferred directory

    databaseClient = await getDatabaseClient();

    try {
        const downloadedFilePath = `downloads/2025-01-10_19-49-42_shm.jpg`;
        //const downloadedFilePath = await downloadFile(DOWNLOAD_URL, saveDirectory);
        console.log(`File downloaded to: ${downloadedFilePath}`);

        // Open the file, and extract a buffer of yesterday's pixel data
        const x1 = 60, y1 = 30, x2 = 61, /*541,*/ y2 = 430;
        const pixelData = readPixelData(downloadedFilePath, x1, y1, x2, y2);
        console.log(`Pixel data extracted from ${x1}, ${y1} to ${x2}, ${y2}:`);

        printPixelData(pixelData, x2 - x1, y2 - y1);

        process.exit(1);

        // Read the color-strip
        const x = 1510; // Vertical strip X coordinate
        const yStart = 116; // Start of the strip
        const yEnd = 422; // End of the strip
        let verticalStrip = [];

        try {
            verticalStrip = readVerticalStrip(downloadedFilePath, x, yStart, yEnd);
            console.log(`Extracted ${verticalStrip.length} pixels from vertical strip at x=${x}`);
            // console.log('Sample pixel data:', verticalStrip.slice(0, 5)); // Show first 5 pixels
        } catch (error) {
            console.error('Error reading vertical strip:', error.message);
        }

        // Translate one vertical strip to a normalized value
        // const value = matchColorToValue(pixelData[0], verticalStrip);
        // console.log(`Normalized value for the input pixel: ${value}`);

        const startDate = new Date(); // Starting date for the iteration
        const timeBetweenX = 60000; // 1 minute between each x-pixel value (in ms)

        // Call the iterator function with the above parameters
        await iterateAndMatchColors(startDate, timeBetweenX, pixelData, verticalStrip, writeDataPoint, x2 - x1, y2 - y1);

        databaseClient.end();

    } catch (error) {
        console.error(error.stack);
    }
})();
