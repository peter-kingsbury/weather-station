#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include <Adafruit_SGP30.h>
#include <Adafruit_TSL2591.h>
#include <Adafruit_LTR390.h>
#include <RW_PMSA003I.h>
#include <RTClib.h>
#include <SPI.h>
#include <RH_RF95.h>
#include "ArduinoJson-v7.2.0.h"

// Define the pins used by the transceiver module
#define RFM95_CS     16
#define RFM95_RST    17
#define RFM95_INT    21

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

// Frequency of the LoRa module (in MHz)
#define RF95_FREQ 915.0

// Singleton instance of the radio driver and sensors
RH_RF95 rf95(RFM95_CS, RFM95_INT);
Adafruit_BME680 bme; // I2C
Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);
Adafruit_LTR390 ltr = Adafruit_LTR390();
uint16_t full_spectrum = 0;

////////////////////////////////////////////////////////////////////////

void setupBME680() {
  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms

  Serial.println("BME680 sensor initialized.");
}

////////////////////////////////////////////////////////////////////////

void setupTSL2591() {
  if (!tsl.begin()) {
    Serial.println("No TSL2591 sensor found ... check your wiring!");
    while (1);
  }

  // Display sensor details
  sensor_t sensor;
  tsl.getSensor(&sensor);
  // Serial.println("------------------------------------");
  // Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  // Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  // Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  // Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" lux");
  // Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" lux");
  // Serial.print  ("Resolution:   "); Serial.print(sensor.resolution, 4); Serial.println(" lux");  
  // Serial.println("------------------------------------");
  // Serial.println("");

  // You can change the gain on the fly, to adapt to brighter/dimmer light situations:
  tsl.setGain(TSL2591_GAIN_MED);  // 1x, 25x, 428x, 9876x

  // Changing the integration time gives you a longer time over which to sense light
  tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest, medium, longest, etc
}

////////////////////////////////////////////////////////////////////////

void setupLTR390() {
  if (!ltr.begin()) {
    Serial.println("No LTR390 sensor found ... check your wiring!");
    while (1);
  }

  // Display sensor details
  // Serial.println("------------------------------------");
  // Serial.println("LTR390 sensor found!");
  // Serial.println("------------------------------------");
  // Serial.println("");

  // Set mode to UV sensing
  ltr.setMode(LTR390_MODE_UVS); // UV mode
  ltr.setGain(LTR390_GAIN_1); // Adjust gain as necessary
  ltr.setResolution(LTR390_RESOLUTION_16BIT); // Adjust resolution as necessary
}

////////////////////////////////////////////////////////////////////////

void setupLoRa() {
  Serial.println("LoRa Transmitter");

  // Initialize the LoRa module
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  if (!rf95.init()) {
    Serial.println("LoRa initialization failed!");
    while (1);
  }

  // Set frequency
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("Setting frequency failed!");
    while (1);
  }
  Serial.print("Set frequency to ");
  Serial.println(RF95_FREQ);

  // Set transmit power (max is 23dBm)
  rf95.setTxPower(23, false);

  // Optional: Set additional LoRa parameters
  rf95.setSignalBandwidth(125000); // Bandwidth in Hz (e.g., 125kHz)
  rf95.setSpreadingFactor(7); // Spreading Factor (6-12)
  rf95.setCodingRate4(5); // Coding Rate (5-8)
  rf95.setPreambleLength(8); // Preamble length (default is 8)
  // rf95.setSyncWord(0x12); // Sync word (default is 0x12)
  rf95.setPayloadCRC(true);

  Serial.println("LoRa setup complete.");
}

////////////////////////////////////////////////////////////////////////

void setup() {
  // Initialize sensors
  // initBME280();
  // initAnemometer();

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("Starting up...");
  
  setupLoRa();
  setupBME680();
  setupTSL2591();
  setupLTR390();

  pinMode(LED_BUILTIN, OUTPUT);
}

////////////////////////////////////////////////////////////////////////

void loop() {
  // Perform sensor readings
  bme.performReading();
  full_spectrum = tsl.getFullLuminosity();

  JsonDocument metrics;

  // Collect data from sensors
  metrics["tmp"] = readTemperature();
  metrics["hum"] = readHumidity();
  metrics["prs"] = readPressure();
  metrics["gas"] = readGasResistance();
  metrics["alt"] = readAltitude();
  metrics["voc"] = readVOC();
  metrics["lum"] = readLuminosity();
  metrics["inf"] = readInfrared();
  metrics["vis"] = readVisibleLight();
  metrics["lux"] = readLux();
  metrics["uva"] = readUV();
  metrics["dst"] = readDust();
  metrics["wsp"] = readWindSpeed();
  metrics["wdr"] = readWindDirection();
  metrics["rnl"] = readRainLevel();
  metrics["rad"] = readRadiationLevel();

  char output[256];
  serializeJson(metrics, output);


  /*float temperature = readTemperature();
  float humidity = readHumidity();
  float pressure = readPressure();
  float gas = readGasResistance();
  float altitude = readAltitude();
  float voc = readVOC();
  float luminosity = readLuminosity();
  float infrared = readInfrared();
  float visible = readVisibleLight();
  float lux = readLux();
  float uv = readUV();
  float dust = readDust();
  float windSpeed = readWindSpeed();
  float windDirection = readWindDirection();
  float rainLevel = readRainLevel();
  float radiationLevel = readRadiationLevel();

  float sensorValues[] = {
    temperature,
    humidity,
    pressure,
    gas,
    altitude,
    voc,
    luminosity,
    infrared,
    visible,
    lux,
    dust,
    windSpeed,
    windDirection,
    rainLevel,
    radiationLevel
  };

  String packet;
  for (int i = 0; i < sizeof(sensorValues) / sizeof(sensorValues[0]); i++) {
    if (i > 0) {
      packet += ",";
    }
    packet += String(sensorValues[i]);
  }*/

  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)

  // Debug print the packet
//  Serial.println(packet);

  Serial.println(output);

  rf95.send((uint8_t *)output, strlen(output));

  // Wait for the message to be sent
  // rf95.waitPacketSent();

  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  
  // Add a delay to control the transmission frequency
  delay(25); // Transmit every second
}

////////////////////////////////////////////////////////////////////////

float readTemperature() {
  return bme.temperature;
}

////////////////////////////////////////////////////////////////////////

float readHumidity() {
  return bme.humidity;
}

////////////////////////////////////////////////////////////////////////

float readPressure() {
  return bme.pressure / 100.0;
}

////////////////////////////////////////////////////////////////////////

float readGasResistance() {
  return bme.gas_resistance / 1000.0;
}

////////////////////////////////////////////////////////////////////////

float readAltitude() {
  return bme.readAltitude(SEALEVELPRESSURE_HPA);
}

////////////////////////////////////////////////////////////////////////

float readVOC() {
  // Add code to read VOC from SGP30
  return 400.0; // Placeholder value
}

////////////////////////////////////////////////////////////////////////

uint16_t readLuminosity() {
  return full_spectrum;
}

uint16_t readInfrared() {
  return full_spectrum >> 16;
}

uint16_t readVisibleLight() {
  return full_spectrum & 0xFFFF;
}

uint16_t readLux() {
  return tsl.calculateLux(full_spectrum & 0xFFFF, full_spectrum >> 16);
}

////////////////////////////////////////////////////////////////////////

float readUV() {
  return ltr.readUVS();
}

////////////////////////////////////////////////////////////////////////

float readDust() {
  // Add code to read dust from PMSA003I
  return 25.0; // Placeholder value
}

////////////////////////////////////////////////////////////////////////

float readWindSpeed() {
  // Add code to read wind speed from anemometer
  return 5.0; // Placeholder value
}

////////////////////////////////////////////////////////////////////////

float readWindDirection() {
  // Add code to read wind direction from wind vane
  return 180.0; // Placeholder value
}

////////////////////////////////////////////////////////////////////////

float readRainLevel() {
  // Add code to read rain level from rain gauge
  return 10.0; // Placeholder value
}

////////////////////////////////////////////////////////////////////////

float readRadiationLevel() {
  // Add code to read radiation level from SBM-20
  return 0.1; // Placeholder value
}

////////////////////////////////////////////////////////////////////////
