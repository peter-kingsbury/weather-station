# Weather Station Project

This project aims to create a comprehensive weather station using an Adafruit Feather RP2040 with LoRa communication, various sensors, and a PostgreSQL database for data storage.

# Server

```mermaid
sequenceDiagram
    participant User as User
    participant Grafana as Grafana
    participant Prometheus as Prometheus
    participant DB as TimeScaleDB
    participant Server as Node.js Server
    participant LoRa as LoRa Radio
    participant RP2040 as RP2040 Microcontroller

    RP2040 ->>+ Sensors: Read sensor data
    Sensors ->>- RP2040: Return data over serial
    RP2040->>LoRa: Send sensor data
    LoRa->>Server: Transmit data
    Server ->> Server: Scrape Schumann data
    Server ->> Server: Collect Weather API data
    Server->>DB: Write data to TimeScaleDB
    Prometheus-->>DB: Scrape metrics
    Grafana-->>Prometheus: Query metrics
    User->>Grafana: View data visualization
```

# Client

* Arduino IDE
* RP2040

## To upload new code

1. Put the RP2040 in BOOTSEL mode (hold BOOT while connecting USB).
2. Perform the compile/upload in the Arduino IDE.
3. That's it!

