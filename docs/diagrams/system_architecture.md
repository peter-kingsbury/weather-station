# System Architecture Diagram

```mermaid
graph TD
  A[Weather Station] -->|LoRa| B[Server]
  B --> C[Database]
```
