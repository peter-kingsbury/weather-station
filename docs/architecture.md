# Architecture Overview

This document provides an overview of the architecture of the weather station project.

```mermaid
graph TD
  subgraph Weather Station
    Arduino[Arduino Uno]
    BME280[Adafruit BME280]
    SGP30[Adafruit SGP30]
    TSL2591[Adafruit TSL2591]
    PMSA003I[Adafruit PMSA003I]
    Anemometer[Anemometer]
    WindVane[Wind Vane]
    RainGauge[Rain Gauge]
    RFM95W[Adafruit RFM95W LoRa Module]
    PowerBoost[Adafruit PowerBoost 1000C]
    SolarPanel[Solar Panel]
    LiPo[LiPo Battery]
    RTC[Adafruit DS3231]

    Arduino -->|I2C| BME280
    Arduino -->|I2C| SGP30
    Arduino -->|I2C| TSL2591
    Arduino -->|UART| PMSA003I
    Arduino -->|Analog| Anemometer
    Arduino -->|Analog| WindVane
    Arduino -->|Digital| RainGauge
    Arduino -->|SPI| RFM95W
    Arduino -->|Power| PowerBoost
    PowerBoost -->|Charge| LiPo
    SolarPanel -->|Charge| PowerBoost
    Arduino -->|I2C| RTC
  end

  subgraph Host Weather Station
    LoRaReceiver[LoRa Receiver]
    Database[PostgreSQL Database]
    Server[Server Raspberry Pi]
  end

  RFM95W -->|LoRa| LoRaReceiver
  LoRaReceiver -->|Data| Server
  Server -->|Store| Database
```
