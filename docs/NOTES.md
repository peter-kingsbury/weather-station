# Pinout Diagram

![img.png](img.png)

# References

* https://learn.adafruit.com/feather-rp2040-rfm95?view=all
* https://docs.circuitpython.org/projects/rfm9x/en/latest/api.html
* https://github.com/adafruit/Adafruit_CircuitPython_RFM9x

